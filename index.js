const express = require("express");
const app = express();


const router = require('./router');
const logger = require('./middlewares');

app.use(express.json());

app.use(logger);

app.use('/api/files', router);

app.use(express.static("./api/files"));

app.use((err, req, res, next) => {
  res.status(500).json({
    message: "Server error"
  });
});

app.listen(8080, () => {
  console.log("Server works at 8080!");
});