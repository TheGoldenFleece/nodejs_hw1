const express = require('express');

module.exports = logger = (req, res, next) => {
  console.log(`${req.method} ${req.url}`, req.body, res.statusCode);
  next();
}