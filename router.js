const express = require('express');
const router = express.Router();

let fs = require("fs");
const re = /\.(log|txt|json|yaml|xml|js)$/ig;

router.post('/', (req, res) => {

  if (!req.body.content && !req.body.filename) {
    res.status(400).json({
      message: `Please specify 'filename' and 'content' parameters`,
    });
  }

  if (!req.body.filename) {
    res.status(400).json({
      message: `Please specify 'filename' parameter`,
    });
  }

  if (!re.test(req.body.filename)) {
    res.status(400).json({
      message: `Application support only log, txt, json, yaml, xml, js file extensions`,
    });
  }

  if (!req.body.content) {
    res.status(400).json({
      message: `Please specify 'content' parameter`,
    });
  }

  if (!fs.existsSync('api/files')) {
    fs.mkdirSync('api/files', {
      recursive: true
    });
  }

  res.status(200).json({
    message: "File created successfully",
  });

  const file = fs.writeFileSync(
    `./api/files/${req.body.filename}`,
    `${req.body.content}`,
    "utf-8"
  );

  file();
});


router.get('/:filename', (req, res) => {

  if (!fs.existsSync('api/files')) {
    res.status(400).json({
      message: `api/files directory is not created yet`
    });
  }

  const dir = './api/files/';

  const file = fs.readdirSync(dir).find(file => file == req.params.filename);

  if (!file) {
    res.status(400).json({
      message: `No file with '${req.params.filename}' filename found`
    });
  }

  res.status(200).json({
    message: "Success",
    filename: file,
    content: `${fs.readFileSync(`${dir}${req.params.filename}`)}`,
    extension: file.match(re)[0].slice(1),
    uploadedDate: fs.statSync(`${dir}${req.params.filename}`).birthtime
  });
});



router.get('/', (req, res) => {
  if (!fs.existsSync('api/files')) {
    res.status(400).json({
      message: `api/files directory is not created yet`
    });
  }


  const dir = './api/files';
  const files = fs.readdirSync(dir);

  if (!files.length) {
    res.status(400).json({
      message: "Client error",
    });
  }

  res.status(200).json({
    message: "Success",
    files
  });
});

module.exports = router;